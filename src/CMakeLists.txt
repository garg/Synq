set( libsynq_SRCS
    synchelper.cpp
)
set( libsynq_HDRS
    synchelper.h
)

kde4_add_library( libsynq  SHARED ${libsynq_SRCS} )

set(QT_USE_DBUS TRUE)
set(QT_WRAP_CPP TRUE)
include( ${QT_USE_FILE} )
qt4_wrap_cpp(libsynq_SRCS libsynq_HDRS)
target_link_libraries(libsynq ${QT_QTCORE_LIBRARY})
