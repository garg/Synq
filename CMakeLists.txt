cmake_minimum_required(VERSION 2.8)
project( synq )

find_package (Qt4 REQUIRED)
find_package (KDE4 REQUIRED)

add_definitions(${QT_DEFINITIONS})
include_directories(${QT_INCLUDES})

add_subdirectory(src)
